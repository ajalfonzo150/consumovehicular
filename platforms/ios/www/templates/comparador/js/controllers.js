// Controller of menu toggle.
// Learn more about Sidenav directive of angular material
// https://material.angularjs.org/latest/#/demo/material.components.sidenav
appControllers.controller('ComparerCtrl', function ($scope,$ionicModal, $timeout, $mdUtil, $mdSidenav, $log, $ionicHistory, $state, $ionicPlatform, $mdDialog, $mdBottomSheet, $mdMenu, $mdSelect, $ionicLoading,HostService,$ionicPopup) {
    
    $scope.toggleLeft = buildToggler('left');

    // buildToggler is for create menu toggle.
    // Parameter :  
    // navID = id of navigation bar.
    function buildToggler(navID) {
        var debounceFn = $mdUtil.debounce(function () {
            $mdSidenav(navID).toggle();
        }, 0);
        return debounceFn;
    };// End buildToggler.

    // navigateTo is for navigate to other page 
    // by using targetPage to be the destination state. 
    // Parameter :  
    // stateNames = target state to go
    $scope.navigateTo = function (stateName) {
        $timeout(function () {
            $mdSidenav('left').close();
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
                $state.go(stateName);
            }
        }, ($scope.isAndroid == false ? 300 : 0));
    };// End navigateTo.

    //closeSideNav is for close side navigation
    //It will use with event on-swipe-left="closeSideNav()" on-drag-left="closeSideNav()"
    //When user swipe or drag md-sidenav to left side
    $scope.closeSideNav = function(){
        $mdSidenav('left').close();
    };
    //End closeSideNav

    //  $ionicPlatform.registerBackButtonAction(callback, priority, [actionId])
    //
    //     Register a hardware back button action. Only one action will execute
    //  when the back button is clicked, so this method decides which of
    //  the registered back button actions has the highest priority.
    //
    //     For example, if an actionsheet is showing, the back button should
    //  close the actionsheet, but it should not also go back a page view
    //  or close a modal which may be open.
    //
    //  The priorities for the existing back button hooks are as follows:
    //  Return to previous view = 100
    //  Close side menu         = 150
    //  Dismiss modal           = 200
    //  Close action sheet      = 300
    //  Dismiss popup           = 400
    //  Dismiss loading overlay = 500
    //
    //  Your back button action will override each of the above actions
    //  whose priority is less than the priority you provide. For example,
    //  an action assigned a priority of 101 will override the ‘return to
    //  previous view’ action, but not any of the other actions.
    //
    //  Learn more at : http://ionicframework.com/docs/api/service/$ionicPlatform/#registerBackButtonAction

    $ionicPlatform.registerBackButtonAction(function(){

        if($mdSidenav("left").isOpen()){
            //If side navigation is open it will close and then return
            $mdSidenav('left').close();
        }
        else if(jQuery('md-bottom-sheet').length > 0 ) {
            //If bottom sheet is open it will close and then return
            $mdBottomSheet.cancel();
        }
        else if(jQuery('[id^=dialog]').length > 0 ){
            //If popup dialog is open it will close and then return
            $mdDialog.cancel();
        }
        else if(jQuery('md-menu-content').length > 0 ){
            //If md-menu is open it will close and then return
            $mdMenu.hide();
        }
        else if(jQuery('md-select-menu').length > 0 ){
            //If md-select is open it will close and then return
            $mdSelect.hide();
        }

        else{

            // If control :
            // side navigation,
            // bottom sheet,
            // popup dialog,
            // md-menu,
            // md-select
            // is not opening, It will show $mdDialog to ask for
            // Confirmation to close the application or go to the view of lasted state.

            // Check for the current state that not have previous state.
            // It will show $mdDialog to ask for Confirmation to close the application.

            if($ionicHistory.backView() == null){

                //Check is popup dialog is not open.
                if(jQuery('[id^=dialog]').length == 0 ) {

                    // mdDialog for show $mdDialog to ask for
                    // Confirmation to close the application.

                    $mdDialog.show({
                        controller: 'DialogController',
                        templateUrl: 'confirm-dialog.html',
                        targetEvent: null,
                        locals: {
                            displayOption: {
                                title: "Confirmación",
                                content: "¿Quiere salir de la aplicación?",
                                ok: "CONFIRMAR",
                                cancel: "CANCELAR"
                            }
                        }
                    }).then(function () {
                        //If user tap Confirm at the popup dialog.
                        //Application will close.
                        ionic.Platform.exitApp();
                    }, function () {
                        // For cancel button actions.
                    }); //End mdDialog
                }
            }
            else{
                //Go to the view of lasted state.
                $ionicHistory.goBack();
            }
        }

    },100);
    //End of $ionicPlatform.registerBackButtonAction

    $scope.marcas=[];
    $scope.modelos=[];
    
    $scope.tipos=[];
    
    $scope.marcas2=[];
    $scope.modelos2=[];

    $scope.combustible=[];
    $scope.combustible2=[];
    
    $scope.selectmodelo=0;
    $scope.selectmodelo2=0;

    $scope.cars=[];
    //$scope.car1={co2: 0, rendimientoCarretera: 0, rendimientoElectrico: 0, rendimientoMixto: 0, rendimientoPonderadoCombustible: 0, rendimientoPonderadoElectrico: 0,rendimientoUrbano: 0, tipo: "", traccion: "", transmision: "", valorMedidoNox: 0};
    //$scope.car2={co2: 0, rendimientoCarretera: 0, rendimientoElectrico: 0, rendimientoMixto: 0, rendimientoPonderadoCombustible: 0, rendimientoPonderadoElectrico: 0,rendimientoUrbano: 0, tipo: "", traccion: "", transmision: "", valorMedidoNox: 0};
    $scope.newcars={
        marca:{},
        modelo:{},
        propulsion:{}
    };
    $scope.car1={};
    $scope.car2={};
    $scope.carro1 = {};
    $scope.carro2 = {};
    $scope.car1.rendimientoPonderadoCombustibleA=0;
    $scope.car2.rendimientoPonderadoCombustibleA=0;
    var selectcars=0;
    $scope.etiqueta={};
    function loadRemoteData() {
        // The friendService returns a promise.
        HostService.getMarcas()
            .then(
                function( response ) {
                    $scope.marcas = response;
                    $scope.marcas2 = response;
                    console.log(response);
                }
            )
        ;
    }

    $scope.openCity = function(evt, cityName)
    {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    loadRemoteData();
       $scope.loadModelo = function(id) {
        console.log(id,"modelo");
         HostService.getModelo(id.idMarca)
            .then(
                function( response ) {
                    $scope.modelos = response;
                    $scope.combustible=[];
                    console.log(response);
                }
            )
        ;
       
    };

    $scope.loadModelo2 = function(id) {
         HostService.getModelo(id.idMarca)
            .then(
                function( response ) {
                    $scope.modelos2 = response;
                    $scope.combustible2=[];
                    console.log(response);
                }
            )
        ;
       
    };

    $scope.GetCars = function(id) {
        $scope.selectmodelo = id.idModelo;
        HostService.getCombustible(id.idModelo)
            .then(
                function( response ) {
                    console.log(response,"combustible");
                    $scope.combustible = response;
                }
            )
        ;
         

       
    };

    $scope.Filtrar = function(id) {
        $scope.cars=[];
        HostService.getAllCars($scope.selectmodelo )
            .then(
                function( response ) {
                    selectcars=1;
                    $scope.lstcars=[];
                    angular.forEach(response, function(item, i) {
                        
                        if(item.idEtiqueta==id.idEtiqueta){
                            $scope.lstcars.push(item);
                        }
                    });
                    console.log($scope.lstcars,"lista de carrooos");
                    $scope.openModalCars($scope.lstcars);
                    console.log(response);
                }
            )
        ;
        
    };

    $scope.GetCars2 = function(id) {
        $scope.selectmodelo2 = id.idModelo;
        HostService.getCombustible(id.idModelo)
            .then(
                function( response ) {
                    $scope.combustible2 = response;
                }
            )
        ;
        
       
    };

    $scope.Filtrar2 = function(id) {
        $scope.cars=[];
        HostService.getAllCars($scope.selectmodelo2)
            .then(
                function( response ) {
                    selectcars=2;
                    $scope.lstcars=[];
                    angular.forEach(response, function(item, i) {
                        console.log(item,id.idEtiqueta);
                        if(item.idEtiqueta==id.idEtiqueta){
                            $scope.lstcars.push(item);
                        }
                    });
                    console.log($scope.lstcars,"lista de carrooos");
                    $scope.openModalCars($scope.lstcars);
                    console.log(response);
                }
            )
        ;
        
    };


    $scope.disableoption = function(val1,val2) {
        console.log(val1);
        console.log(val2);
        if(val1==val2){
            return true;
        }
        else
        {
            return false;
        }
      };

       $scope.showPopup = function(title,template) {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template,
            cancelText:"Cancelar",
            okText: "Aceptar"
        });
 }

    $ionicModal.fromTemplateUrl('templates/comparador/html/viewcars.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalCars = modal;
    });

    $scope.openModalCars = function(lstcars) {
        $scope.lstcars = lstcars;
        if($scope.lstcars.length==0)
        {
            $scope.$parent.showPopup('Comparador','No hay Vehículo registrados para los filtros seleccionados');
        }
        else
        {
            console.log("show");
            $scope.modalCars.show();
        }
    };

    $scope.paramCompare=[
    {
        Nombre: "Precio kWh",
        Valor: 100
      },{
        Nombre: "Precio diesel",
        Valor: 600
      },
      {
        Nombre: "Precio bencina",
        Valor: 800
      },
      {
        Nombre:"Motor electrico",
        Valor:90
      },
      {
        Nombre:"Motor combustion",
        Valor:10
      },
      {
        Nombre: "Kilometros anuales",
        Valor: 20000
      }
      ];

      /*
      idEtiqueta=='COMBUSTION DIESEL',
  ""COMBUSTION GASOLINA"",
  ""HIBRIDO ENCHUFABLE DIESEL"",
  ""HIBRIDO DIESEL"",
  ""HIBRIDO GASOLINA"",
  ""HIBRIDO ENCHUFABLE GASOLINA"",
  */
      $scope.gastosAdicionalesCar1 =0 ;
      $scope.gastosAdicionalesCar2 =0 ;
     $scope.Calcular = function() {
        
            if($scope.car1.co2>=0){
                $scope.car1.tnco2 = ($scope.car1.co2*$scope.paramCompare[5].Valor)/1000000;
                if($scope.car1.idEtiqueta=="COMBUSTION GASOLINA")
                {
                    $scope.car1.rendimientoPonderadoCombustibleA=($scope.paramCompare[5].Valor/$scope.car1.rendimientoMixto) * $scope.paramCompare[2].Valor;
                }
                if($scope.car1.idEtiqueta=="COMBUSTION DIESEL")
                {
                    $scope.car1.rendimientoPonderadoCombustibleA=($scope.paramCompare[5].Valor/$scope.car1.rendimientoMixto) * $scope.paramCompare[1].Valor;
                }
                if($scope.car1.idEtiqueta=="HIBRIDO SIN RECARGA EXTERIOR DIESEL") // || $scope.car1.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR DIESEL"
                {
                    $scope.car1.rendimientoPonderadoCombustibleA=($scope.paramCompare[5].Valor/($scope.car1.rendimientoPonderadoElectrico/2)* $scope.paramCompare[0].Valor)+($scope.paramCompare[5].Valor/($scope.car1.rendimientoMixto/2)* $scope.paramCompare[1].Valor);
                    $scope.car1.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/($scope.car1.rendimientoMixto)) * $scope.paramCompare[1].Valor);
                }
                if($scope.car1.idEtiqueta=="HIBRIDO SIN RECARGA EXTERIOR GASOLINA")  //  || $scope.car1.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR GASOLINA"
                {
                    $scope.car1.rendimientoPonderadoCombustibleA=($scope.paramCompare[5].Valor/($scope.car1.rendimientoElectrico/2)* $scope.paramCompare[0].Valor)+($scope.paramCompare[5].Valor/($scope.car1.rendimientoMixto/2)* $scope.paramCompare[2].Valor);
                    $scope.car1.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/($scope.car1.rendimientoMixto)) * $scope.paramCompare[2].Valor);
                }
                if($scope.car1.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR DIESEL")
                {
                    //$scope.car1.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/$scope.car1.rendimientoPonderadoElectrico)* $scope.paramCompare[0].Valor)+(($scope.paramCompare[5].Valor/$scope.car1.rendimientoPonderadoCombustible)* $scope.paramCompare[1].Valor);
                    $scope.car1.rendimientoPonderadoCombustibleA=((($scope.paramCompare[5].Valor/($scope.car1.rendimientoPonderadoElectrico))* $scope.paramCompare[0].Valor)/100* $scope.paramCompare[3].Valor)+((($scope.paramCompare[5].Valor/($scope.car1.rendimientoPonderadoCombustible))* $scope.paramCompare[1].Valor)/100* $scope.paramCompare[4].Valor);
                }
                if($scope.car1.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR GASOLINA")
                {
                    $scope.car1.rendimientoPonderadoCombustibleA=((($scope.paramCompare[5].Valor/($scope.car1.rendimientoPonderadoElectrico))* $scope.paramCompare[0].Valor)/100* $scope.paramCompare[3].Valor)+((($scope.paramCompare[5].Valor/($scope.car1.rendimientoPonderadoCombustible))* $scope.paramCompare[2].Valor)/100* $scope.paramCompare[4].Valor);
                }
                if($scope.car1.idEtiqueta=="ELECTRICO PURO ")
                {
                    $scope.car1.rendimientoPonderadoCombustibleA=($scope.paramCompare[5].Valor/$scope.car1.rendimientoElectrico)* $scope.paramCompare[0].Valor;
                }
            }
            if($scope.car2.co2>=0){
                $scope.car2.tnco2 = ($scope.car2.co2*$scope.paramCompare[5].Valor)/1000000;
                
                if($scope.car2.idEtiqueta=="COMBUSTION GASOLINA")
                {
                    $scope.car2.rendimientoPonderadoCombustibleA=($scope.paramCompare[5].Valor/$scope.car2.rendimientoMixto)* $scope.paramCompare[2].Valor;
                }
                if($scope.car2.idEtiqueta=="COMBUSTION DIESEL")
                {
                    $scope.car2.rendimientoPonderadoCombustibleA=($scope.paramCompare[5].Valor/$scope.car2.rendimientoMixto)* $scope.paramCompare[1].Valor;
                }
                if($scope.car2.idEtiqueta=="HIBRIDO SIN RECARGA EXTERIOR DIESEL" || $scope.car2.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR DIESEL")
                {
                    $scope.car2.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/($scope.car2.rendimientoMixto/2))* $scope.paramCompare[0].Valor)+(($scope.paramCompare[5].Valor/($scope.car2.rendimientoMixto/2))* $scope.paramCompare[1].Valor);
                    $scope.car2.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/($scope.car2.rendimientoMixto))* $scope.paramCompare[1].Valor);
                }
                if($scope.car2.idEtiqueta=="HIBRIDO SIN RECARGA EXTERIOR GASOLINA" || $scope.car2.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR GASOLINA")
                {
                    $scope.car2.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/($scope.car2.rendimientoElectrico/2))* $scope.paramCompare[0].Valor)+(($scope.paramCompare[5].Valor/($scope.car2.rendimientoMixto/2))* $scope.paramCompare[2].Valor);
                    $scope.car2.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/($scope.car2.rendimientoMixto))* $scope.paramCompare[2].Valor);
                }
                if($scope.car2.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR DIESEL")
                {
                    //$scope.car2.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/($scope.car2.rendimientoPonderadoElectrico/2))* $scope.paramCompare[0].Valor)+(($scope.paramCompare[5].Valor/($scope.car2.rendimientoPonderadoCombustible/2))* $scope.paramCompare[1].Valor);
                    $scope.car2.rendimientoPonderadoCombustibleA=((($scope.paramCompare[5].Valor/($scope.car2.rendimientoPonderadoElectrico)* $scope.paramCompare[0].Valor)/100* $scope.paramCompare[3].Valor)+((($scope.paramCompare[5].Valor/($scope.car2.rendimientoPonderadoCombustible))* $scope.paramCompare[1].Valor))/100 * $scope.paramCompare[4].Valor);
                    
                }
                if($scope.car2.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR GASOLINA")
                {
                    $scope.car2.rendimientoPonderadoCombustibleA=((($scope.paramCompare[5].Valor/($scope.car2.rendimientoPonderadoElectrico))* $scope.paramCompare[0].Valor)/100 * $scope.paramCompare[3].Valor)+((($scope.paramCompare[5].Valor/($scope.car2.rendimientoPonderadoCombustible))* $scope.paramCompare[2].Valor)/100 * $scope.paramCompare[4].Valor);
                    
                }
                if($scope.car2.idEtiqueta=="ELECTRICO PURO ")
                {
                    $scope.car2.rendimientoPonderadoCombustibleA=(($scope.paramCompare[5].Valor/$scope.car2.rendimientoElectrico))* $scope.paramCompare[0].Valor;
                }
            }
            if($scope.car1.rendimientoPonderadoCombustibleA<$scope.car2.rendimientoPonderadoCombustibleA)
            {
                console.log("caso1 ");
                $scope.gastosAdicionalesCar2 =$scope.car2.rendimientoPonderadoCombustibleA-$scope.car1.rendimientoPonderadoCombustibleA;
                $scope.gastosAdicionalesCar1=0;
            }
            if($scope.car1.rendimientoPonderadoCombustibleA>$scope.car2.rendimientoPonderadoCombustibleA)
            {
                console.log("caso 2");
                $scope.gastosAdicionalesCar1 =$scope.car1.rendimientoPonderadoCombustibleA-$scope.car2.rendimientoPonderadoCombustibleA;
                $scope.gastosAdicionalesCar2=0;
            }
            if($scope.modalG!=null)
                $scope.modalG.hide();
    };
     
    $scope.closeModalCars = function(car) {
                    console.log(selectcars,car);
                    console.log("seleccat");
                    if(selectcars==1){ $scope.car1=car};
                    if(selectcars==2){ $scope.car2=car};
        HostService.getParamsCompare()
            .then(
                function( response ) {
                    var arra= response;
                    console.log(arra,"array");
                    console.log(arra[0].Valor,"valor");
                    $scope.paramCompare[0].Valor=parseInt(arra[0].Valor);
                    $scope.paramCompare[1].Valor=parseInt(arra[1].Valor);
                    $scope.paramCompare[2].Valor=parseInt(arra[2].Valor);
                    $scope.paramCompare[3].Valor=parseInt(arra[3].Valor);
                    $scope.paramCompare[4].Valor=parseInt(arra[4].Valor);
                    $scope.paramCompare[5].Valor=parseInt(arra[5].Valor);
                    console.log($scope.paramCompare,"cargarParam");
                    $scope.Calcular();
                }
            )
        ;
      $scope.modalCars.hide();
    };

   
     $scope.SearchLabel = function(item) {
        $scope.car = item;
        console.log(item,"carrroooooooo etiqueta");
        if($scope.car.idEtiqueta=="COMBUSTION GASOLINA")
        {
            $scope.etiqueta.titulo="titlecombg";
            $scope.etiqueta.pie="etiqueta-footcomb";
            $scope.etiqueta.body ="etiqueta-bodycomb";
            $scope.etiqueta.ciudad ="ciudad";

        }
        if($scope.car.idEtiqueta=="COMBUSTION DIESEL")
        {
            $scope.etiqueta.titulo="titlecombd";
            $scope.etiqueta.pie="etiqueta-footcomb";
            $scope.etiqueta.body ="etiqueta-bodycomb";
            $scope.etiqueta.ciudad ="ciudad";
        }
        if($scope.car.idEtiqueta=="HIBRIDO SIN RECARGA EXTERIOR DIESEL")
        {
            $scope.etiqueta.titulo="titlehibsd";
            $scope.etiqueta.pie="etiqueta-foothibs";
            $scope.etiqueta.body ="etiqueta-body";
            $scope.etiqueta.ciudad ="ciudad";
        }
        if($scope.car.idEtiqueta=="HIBRIDO SIN RECARGA EXTERIOR GASOLINA")
        {
            $scope.etiqueta.titulo="titlehibgs";
            $scope.etiqueta.pie="etiqueta-foothibs";
            $scope.etiqueta.body ="etiqueta-body";
            $scope.etiqueta.ciudad ="ciudad";
        }
        if($scope.car.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR DIESEL")
        {
            $scope.etiqueta.titulo="titlehibcd";
            $scope.etiqueta.pie="etiqueta-foothibc";
            $scope.etiqueta.body ="etiqueta-body";
            $scope.etiqueta.ciudad ="ciudad";
        }
        if($scope.car.idEtiqueta=="HIBRIDO CON RECARGA EXTERIOR GASOLINA")
        {
            $scope.etiqueta.titulo="titlehibgc";
            $scope.etiqueta.pie="etiqueta-foothibc";
            $scope.etiqueta.body ="etiqueta-body";
            $scope.etiqueta.ciudad ="ciudad";
        }
        if($scope.car.idEtiqueta=="ELECTRICO PURO ")
        {
            $scope.etiqueta.titulo="titleelect";
            $scope.etiqueta.pie="etiqueta-footelec";
            $scope.etiqueta.body ="etiqueta-body";
            $scope.etiqueta.ciudad ="ciudad";
        }
        $scope.openModal();
        
    };
    
    $scope.showAditional = function() {
        $scope.showPopup('Gasto adicional','Es la diferencia entre el gasto anual en energía del vehículo que es menos eficiente, respecto del que es más eficiente en su consumo.');
    };
      $ionicModal.fromTemplateUrl('templates/etiquetas/html/viewlabel.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
      $scope.modal.show();
    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };


    $ionicModal.fromTemplateUrl('templates/comparador/html/gastosconfig.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalG = modal;
    });
    $scope.gastosConf = function() {
      $scope.modalG.show();
    };
    $scope.closeModalG = function() {
      $scope.modalG.hide();
    };
}); // End of menu toggle controller.