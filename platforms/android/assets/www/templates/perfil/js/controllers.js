// Controller of menu toggle.
// Learn more about Sidenav directive of angular material
// https://material.angularjs.org/latest/#/demo/material.components.sidenav
appControllers.controller('PerfilCtrl', function ($scope, $timeout, $mdUtil, $ionicPopup, $ionicModal, $mdSidenav, $log, $ionicHistory, $state, $ionicPlatform, $mdDialog, $mdBottomSheet, $mdMenu, $mdSelect, $ionicLoading, UserDat,$cordovaOauth,HostService) {
    
    $scope.toggleLeft = buildToggler('left');
    console.log("fasdfasfd");
    // buildToggler is for create menu toggle.
    // Parameter :  
    // navID = id of navigation bar.
    function buildToggler(navID) {
        var debounceFn = $mdUtil.debounce(function () {
            $mdSidenav(navID).toggle();
        }, 0);
        return debounceFn;
    };// End buildToggler.

    // navigateTo is for navigate to other page 
    // by using targetPage to be the destination state. 
    // Parameter :  
    // stateNames = target state to go
    $scope.navigateTo = function (stateName) {
        $timeout(function () {
            $mdSidenav('left').close();
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
                $state.go(stateName);
            }
        }, ($scope.isAndroid == false ? 300 : 0));
    };// End navigateTo.

    //closeSideNav is for close side navigation
    //It will use with event on-swipe-left="closeSideNav()" on-drag-left="closeSideNav()"
    //When user swipe or drag md-sidenav to left side
    $scope.closeSideNav = function(){
        $mdSidenav('left').close();
    };
    //End closeSideNav

    //  $ionicPlatform.registerBackButtonAction(callback, priority, [actionId])
    //
    //     Register a hardware back button action. Only one action will execute
    //  when the back button is clicked, so this method decides which of
    //  the registered back button actions has the highest priority.
    //
    //     For example, if an actionsheet is showing, the back button should
    //  close the actionsheet, but it should not also go back a page view
    //  or close a modal which may be open.
    //
    //  The priorities for the existing back button hooks are as follows:
    //  Return to previous view = 100
    //  Close side menu         = 150
    //  Dismiss modal           = 200
    //  Close action sheet      = 300
    //  Dismiss popup           = 400
    //  Dismiss loading overlay = 500
    //
    //  Your back button action will override each of the above actions
    //  whose priority is less than the priority you provide. For example,
    //  an action assigned a priority of 101 will override the ‘return to
    //  previous view’ action, but not any of the other actions.
    //
    //  Learn more at : http://ionicframework.com/docs/api/service/$ionicPlatform/#registerBackButtonAction


    $ionicPlatform.registerBackButtonAction(function(){

        if($mdSidenav("left").isOpen()){
            //If side navigation is open it will close and then return
            $mdSidenav('left').close();
        }
        else if(jQuery('md-bottom-sheet').length > 0 ) {
            //If bottom sheet is open it will close and then return
            $mdBottomSheet.cancel();
        }
        else if(jQuery('[id^=dialog]').length > 0 ){
            //If popup dialog is open it will close and then return
            $mdDialog.cancel();
        }
        else if(jQuery('md-menu-content').length > 0 ){
            //If md-menu is open it will close and then return
            $mdMenu.hide();
        }
        else if(jQuery('md-select-menu').length > 0 ){
            //If md-select is open it will close and then return
            $mdSelect.hide();
        }

        else{

            // If control :
            // side navigation,
            // bottom sheet,
            // popup dialog,
            // md-menu,
            // md-select
            // is not opening, It will show $mdDialog to ask for
            // Confirmation to close the application or go to the view of lasted state.

            // Check for the current state that not have previous state.
            // It will show $mdDialog to ask for Confirmation to close the application.

            if($ionicHistory.backView() == null){

                //Check is popup dialog is not open.
                if(jQuery('[id^=dialog]').length == 0 ) {

                    // mdDialog for show $mdDialog to ask for
                    // Confirmation to close the application.

                    $mdDialog.show({
                        controller: 'DialogController',
                        templateUrl: 'confirm-dialog.html',
                        targetEvent: null,
                        locals: {
                            displayOption: {
                                title: "Confirmación",
                                content: "¿Quiere salir de la aplicación?",
                                ok: "CONFIRMAR",
                                cancel: "CANCELAR"
                            }
                        }
                    }).then(function () {
                        //If user tap Confirm at the popup dialog.
                        //Application will close.
                        ionic.Platform.exitApp();
                    }, function () {
                        // For cancel button actions.
                    }); //End mdDialog
                }
            }
            else{
                //Go to the view of lasted state.
                $ionicHistory.goBack();
            }
        }

    },100);
    //End of $ionicPlatform.registerBackButtonAction

    $scope.showPopup = function(title,template) {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template,
            cancelText:"Cancelar",
            okText: "Aceptar"
        });
    }
    
   // if(UserDat!=undefined || UserDat.user !=""){
    //    $scope.user = UserDat;
    //}else{
        //UserDat = JSON.parse(window.localStorage.getItem("UserDat"));
        //$scope.user = UserDat;
    //}
    if (window.localStorage.getItem("UserDat") !="") {
        UserDat = JSON.parse(window.localStorage.getItem("UserDat"));
        $scope.user = UserDat;
        console.log(UserDat);
        //window.localStorage.setItem("UserDat", UserDat);
        //window.localStorage.setItem("token", UserDat.idToke);
        //window.localStorage.setItem("UserD", UserDat.user);
        //window.localStorage.setItem("typelogin", "gp");
    }
    
    console.log($scope.user,"user data");
    // Set Ink

    //Entorno de QA
    // var basedrupal = "http://qa.consumovehicular.cl/";
    
    //Entorno de Produccion
    var basedrupal = "http://www.consumovehicular.cl/";

    $scope.logout = function() {
        console.log('entra a logout y el tipo es: '+UserDat.typelogin);
          var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": basedrupal+"usrapi/user/logout",
                  "method": "POST",
                  "headers": {
                    "x-csrf-token": window.localStorage.getItem("token"),
                    "content-type": "application/json",
                    "cache-control": "no-cache"
                    }
                }
                if(window.localStorage.getItem("typelogin")=="fb"){
                    //window.localStorage.setItem("token", "");
                    facebookConnectPlugin.logout(function(){}, function(){})
                    //$state.go("app.login", {cache: false});
                }
                if(window.localStorage.getItem("typelogin")=="lg"){
                    $.ajax(settings).done(function (response) {
                        $ionicHistory.clearCache();
                        $ionicHistory.clearHistory();
                        $state.go("app.home", {cache: false});
                      }).error(function(response){
                        console.log('error al logout de tipelogin lg');
                      });
                }
                if(window.localStorage.getItem("typelogin")=="gp"){
                    
                }
        
        window.localStorage.setItem("token", "");
        window.localStorage.setItem("UserDat", "");
        window.localStorage.setItem("typelogin", "");
        $state.go("app.home", {cache: false});
    };

    $scope.savechange = function(id) {
          var settings = {
                  "async": true,
                  "crossDomain": true,
                  "url": basedrupal +"usrapi/user/"+UserDat.id,
                  "method": "PUT",
                  "headers": {
                    "x-csrf-token": window.localStorage.getItem("token"),
                    "content-type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache",
                    "postman-token": "6cda7c1b-0a15-c2b8-a2f1-cd78a25b0b1f"
                  },
                  "data": {
                    "field_nombre_completo[und][0][value]": $scope.user.fullname
                  }
                }
                UserDat.fullname=$scope.user.fullname;
                window.localStorage.setItem("UserDat", JSON.stringify($scope.user));
                if(UserDat.typelogin=="lg"){
                    $.ajax(settings).done(function (response) {
                      UserDat.fullname=$scope.user.fullname;
                      $scope.showPopup('Perfil','Se han guardado los cambios.');
                    });
                }
                else
                {
                    $scope.showPopup('Perfil','Se han guardado los cambios.');
                }

       
    };

}); // End of menu toggle controller.