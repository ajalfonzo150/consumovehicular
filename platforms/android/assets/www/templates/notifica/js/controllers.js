// Controller of menu toggle.
// Learn more about Sidenav directive of angular material
// https://material.angularjs.org/latest/#/demo/material.components.sidenav
appControllers.controller('NotificarCtrl', function ($scope, $timeout, $compile, $mdUtil, $mdSidenav, $log, $ionicHistory, $state, $ionicPlatform, $mdDialog, $mdBottomSheet, $mdMenu, $mdSelect, $ionicLoading,HostService,DatRegiones,DatComunas, $http, $ionicPopup, $ionicModal) {
    
    $scope.toggleLeft = buildToggler('left');

    // buildToggler is for create menu toggle.
    // Parameter :  
    // navID = id of navigation bar.
    function buildToggler(navID) {
        var debounceFn = $mdUtil.debounce(function () {
            $mdSidenav(navID).toggle();
        }, 0);
        return debounceFn;
    };// End buildToggler.

    // navigateTo is for navigate to other page 
    // by using targetPage to be the destination state. 
    // Parameter :  
    // stateNames = target state to go
    $scope.navigateTo = function (stateName) {
        $timeout(function () {
            $mdSidenav('left').close();
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
                $state.go(stateName);
            }
        }, ($scope.isAndroid == false ? 300 : 0));
    };// End navigateTo.

    //closeSideNav is for close side navigation
    //It will use with event on-swipe-left="closeSideNav()" on-drag-left="closeSideNav()"
    //When user swipe or drag md-sidenav to left side
    $scope.closeSideNav = function(){
        $mdSidenav('left').close();
    };
    //End closeSideNav

    //  $ionicPlatform.registerBackButtonAction(callback, priority, [actionId])
    //
    //     Register a hardware back button action. Only one action will execute
    //  when the back button is clicked, so this method decides which of
    //  the registered back button actions has the highest priority.
    //
    //     For example, if an actionsheet is showing, the back button should
    //  close the actionsheet, but it should not also go back a page view
    //  or close a modal which may be open.
    //
    //  The priorities for the existing back button hooks are as follows:
    //  Return to previous view = 100
    //  Close side menu         = 150
    //  Dismiss modal           = 200
    //  Close action sheet      = 300
    //  Dismiss popup           = 400
    //  Dismiss loading overlay = 500
    //
    //  Your back button action will override each of the above actions
    //  whose priority is less than the priority you provide. For example,
    //  an action assigned a priority of 101 will override the ‘return to
    //  previous view’ action, but not any of the other actions.
    //
    //  Learn more at : http://ionicframework.com/docs/api/service/$ionicPlatform/#registerBackButtonAction

    $ionicPlatform.registerBackButtonAction(function(){

        if($mdSidenav("left").isOpen()){
            //If side navigation is open it will close and then return
            $mdSidenav('left').close();
        }
        else if(jQuery('md-bottom-sheet').length > 0 ) {
            //If bottom sheet is open it will close and then return
            $mdBottomSheet.cancel();
        }
        else if(jQuery('[id^=dialog]').length > 0 ){
            //If popup dialog is open it will close and then return
            $mdDialog.cancel();
        }
        else if(jQuery('md-menu-content').length > 0 ){
            //If md-menu is open it will close and then return
            $mdMenu.hide();
        }
        else if(jQuery('md-select-menu').length > 0 ){
            //If md-select is open it will close and then return
            $mdSelect.hide();
        }

        else{

            // If control :
            // side navigation,
            // bottom sheet,
            // popup dialog,
            // md-menu,
            // md-select
            // is not opening, It will show $mdDialog to ask for
            // Confirmation to close the application or go to the view of lasted state.

            // Check for the current state that not have previous state.
            // It will show $mdDialog to ask for Confirmation to close the application.

            if($ionicHistory.backView() == null){

                //Check is popup dialog is not open.
                if(jQuery('[id^=dialog]').length == 0 ) {

                    // mdDialog for show $mdDialog to ask for
                    // Confirmation to close the application.

                    $mdDialog.show({
                        controller: 'DialogController',
                        templateUrl: 'confirm-dialog.html',
                        targetEvent: null,
                        locals: {
                            displayOption: {
                                title: "Confirmación",
                                content: "¿Quiere salir de la aplicación?",
                                ok: "CONFIRMAR",
                                cancel: "CANCELAR"
                            }
                        }
                    }).then(function () {
                        //If user tap Confirm at the popup dialog.
                        //Application will close.
                        ionic.Platform.exitApp();
                    }, function () {
                        // For cancel button actions.
                    }); //End mdDialog
                }
            }
            else{
                //Go to the view of lasted state.
                $ionicHistory.goBack();
            }
        }

    },100);
    //End of $ionicPlatform.registerBackButtonAction

    //Si no hay usuario logueado no permito la entrada a esta pagina
    /* if (window.localStorage.getItem("token")!="" && window.localStorage.getItem("token")!=undefined) {
        $scope.showPopup('Inicio de Sesión',"Para acceder al modulo de notificacion, necesita iniciar sesión");
        $scope.navigateTo('app.login');
    }  */

    $scope.denuncia = {
     "idUsuario": "",
     "direccion": "",
     "region": "",
     "comuna": "",
     "observacion": "",
     "coordenada": "",
     "fechaDenuncia": 0
    };

 $scope.showPopup = function(title,template) {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template,
            cancelText:"Cancelar",
            okText: "Aceptar"
        });
 }
 $scope.loadRegion = function(Reg,Com){
    console.log(Reg,Com);
    console.log('Se ejecuta loadRegion');
    $ionicLoading.show({
                    template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
                });
    angular.forEach(DatRegiones, function(item, i) {
        //console.log('Recorre la region: '+ item.name.toUpperCase());
            if(item.name.toUpperCase()== Reg.toUpperCase() || item.name.toUpperCase().includes("METROPOLITANA")){
                $scope.region = item;
                $scope.denuncia.region = item.name;
                angular.forEach(DatComunas, function(itemC, i) {
                        if(itemC.idp==item.id){
                            $scope.comunas.push(itemC);
                        }
                        if(itemC.name.toUpperCase()==Com.toUpperCase()){
                             //$scope.denuncia.comuna = itemC;
                             $scope.denuncia.comuna = itemC.name;
                            $scope.comuna=itemC;
                        }
                    });
                    //$scope.comunas = orderByFilter($scope.comunas, function(item) {
                    //return $scope.comunas.indexOf(item.name);
                  //});
                    console.log($scope.comunas);
                $ionicLoading.hide();
            }
        });
        $ionicLoading.hide();
}

    $scope.init = function() {
        console.log("inicio fde form");
        navigator.geolocation.getCurrentPosition(onSuccess, onError, {
                maximumAge : Infinity,
                timeout : 20000,
                enableHighAccuracy : true
            });
        
    };

    function onError(error) {
        //alert('code: ' + error.code + '\n' +
        //'message: ' + error.message + '\n');
        console.log(error.message,"asdfasfsafasfa");
    }

    function onSuccess(position) {
        console.log("satisfactorio");
        $scope.$apply(function() {
      $scope.lat = position.coords.latitude;
      $scope.lng = position.coords.longitude;

      var geocoder = new google.maps.Geocoder();
      var myLatlng = new google.maps.LatLng($scope.lat, $scope.lng);
      var lat=position.coords.latitude;
        var lang=position.coords.longitude;
        console.log(lang,lat);
        $scope.denuncia.coordenada = lat +","+lang;
        //Google Maps
        //var myLatlng = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
        //var myLatlng = new google.maps.LatLng(43.07493,-89.381388);
        //var myLatlng = new google.maps.LatLng(lat,lang);
        var mapOptions = {
          center: myLatlng,
          zoom: 5,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true

        };
        var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);

        
      var infowindow = new google.maps.InfoWindow(); 
      var marker = new google.maps.Marker();
      var latlng = new google.maps.LatLng($scope.lat, $scope.lng);
      geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            map.fitBounds(results[0].geometry.viewport);
                    marker.setMap(map);
                    marker.setPosition(latlng);
            $scope.denuncia.direccion = results[0].formatted_address;
            
                $timeout(function() {
                    console.log(results[0].address_components[5].long_name);
                    console.log(results[0].address_components[3].long_name);
                    $scope.loadRegion(results[0].address_components[5].long_name,results[0].address_components[3].long_name);
                }, 1000);
            
            console.log($scope.denuncia.direccion,"direccion");
            console.log(results[0],"direccion");

            //infowindow.setContent(results[0].formatted_address);
            //infowindow.open(map, marker);
            google.maps.event.addListener(marker, 'click', function(){
               // infowindow.setContent(results[0].formatted_address);
               // infowindow.open(map, marker);
            });
          } else {
            //alert('No results found');
          }
        } else {
          //alert('Geocoder failed due to: ' + status);
        }
      });
      $scope.map = map;
      console.log(position);
    })
        
    }

   

    



    $scope.loadDireccion = function(){
        var geocoder = new google.maps.Geocoder();
        console.log($scope.denuncia.direccion+","+$scope.denuncia.region+","+$scope.denuncia.comuna,"direccion");
        geocoder.geocode({ 'address': $scope.denuncia.direccion+","+$scope.denuncia.region+","+$scope.denuncia.comuna}, geocodeResult);
    }
    var repeatGeo=false;
    function geocodeResult(results, status) {
        if (status == 'OK') {
            var mapOptions = {
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                  zoom: 5,
                  disableDefaultUI: true
            };
            //map = new google.maps.Map($("#map_canvas").get(0), mapOptions);
            var map = new google.maps.Map(document.getElementById("map"),
            mapOptions);
            map.fitBounds(results[0].geometry.viewport);
            var markerOptions = { position: results[0].geometry.location }
            var marker = new google.maps.Marker(markerOptions);
            marker.setMap(map);
        } else {
            // En caso de no haber resultados o que haya ocurrido un error
            // lanzamos un mensaje con el error
            if(!repeatGeo){
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': $scope.denuncia.comuna + $scope.denuncia.region}, geocodeResult);
            }
            //alert("Geocoding no tuvo éxito debido a: " + status);
        }
    }

    $scope.centerOnMe = function() {
        if(!$scope.map) {
            return;
        }

        $scope.loading = $ionicLoading.show({
          content: 'Getting current location...',
          showBackdrop: false
        });

        navigator.geolocation.getCurrentPosition(function(pos) {
          $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
          $scope.loading.hide();
        }, function(error) {
          console.log('Unable to get location: ' + error.message);
        });
    };


    $scope.map = {
      center: {
        latitude: 21.0000,
        longitude: 78.0000
      },
      zoom: 4,
      events: {
        click: function(mapModel, eventName, originalEventArgs,ok) {
          var e = originalEventArgs[0]; 
            console.log("clikc mapna");
            var geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {

                            console.log(results[1].formatted_address); // details address
                        } else {
                            console.log('Location not found');
                        }
                    } else {
                        console.log('Geocoder failed due to: ' + status);
                    }
                });


        }
      }
    };


    //$scope.list = ListFactory.getList();
    $scope.regions=[];
    $scope.comunas=[];
    $scope.denuncia = {
        "idUsuario": window.localStorage.getItem("UserD"),
        "direccion": "",
        "region": "",
        "comuna": "",
        "observacion": "",
        "coordenada": "",
        "fechaDenuncia": 0
    };

  $scope.regions=DatRegiones;

    
    $scope.SelectRegion = function(id) {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': id.name}, geocodeResult);
        //$scope.denuncia.direccion="";
        $scope.comuna = {};
        $scope.comunas=[];
        $scope.denuncia.comuna = "";
        $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
        });
        $scope.denuncia.region = id.name;
        angular.forEach(DatComunas, function(item, i) {
                if(item.idp==id.id){
                $scope.comunas.push(item);
                }
            });
        //console.log($scope.comunas);
        $ionicLoading.hide();
    };


  

    $scope.SelectComuna = function(id) {
            $scope.denuncia.comuna = id.name;
            var geocoder = new google.maps.Geocoder();
            var direccion = $scope.denuncia.direccion +$scope.denuncia.comuna +","+ $scope.denuncia.region;
            geocoder.geocode({ 'address': direccion}, geocodeResult);
        };

    
  

    $scope.disableoption = function(val1,val2) {
        if(val1==val2){
            return true;
        }
        else
        {
            return false;
        }
      };

      
        $scope.saveDenuncia = function(form) {
            var date = new Date();     // a new date
            var time = date.getTime(); // the timestamp, not neccessarely using UTC as current time
            console.log(time,"time");
           var julian_date = Math.floor((time / 86400000) - (date.getTimezoneOffset()/1440) + 2440587.5);
            
            $scope.denuncia.fechaDenuncia = time;

                var message = "Por favor ingrese los siguientes valores:";
                var validate= false;
                if($scope.denuncia.region=="")
                {
                    message+=" región,";
                    validate = true;
                }
                if($scope.denuncia.comuna=="")
                {
                    message+=" comuna,";
                    validate=true
                }
                if($scope.denuncia.direccion=="")
                {
                    message+=" dirección,";
                    validate=true
                }
                if($scope.denuncia.observacion=="")
                {
                    message+=" observación,";
                    validate=true
                }
                if(validate){
                    message+=".";
                    $scope.showPopup('',message.replace(',.','.'));
                }
                else
                {

                HostService.saveDenuncia($scope.denuncia)
                    .then(
                        function( response ) {
                            console.log(response,"eliminar");

                            if(response.idDenuncia>0){
                                console.log("eliminado");
                                $scope.showPopup('Notificación','Muchas gracias por la notificación! la hemos recibido exitosamente.');
                                //alert("La denuncia fue enviada exitosamente.");
                                //$scope.gotopath("app.denuncia");
                            }

                            console.log(response);
                        }
                    )
                ;
            }
      };

      $scope.keyPressed = function(keyEvent, formModel) {
        if (keyEvent.keyCode == 13) {
            console.log('escribo una letra');
            $scope.saveDenuncia(formModel);
        }
    };

    //codigo para el manejo de los focus en los controles
    //esta funcionando pero hay que cambiar el ng-focus por algo que sea como focus-me
    //porque el ng-focus es algo que se ejecuta cuando el usuario le da foco al input 
   /*  $scope.focusElementComuna = false;
    $scope.focusElementDireccion = false;
    $scope.focusElementObservacion = false;
    $scope.keyPressedRegion = function(keyEvent) {
        console.log('go en region');
        $scope.focusElementComuna=true;
    };
    $scope.keyPressedComuna = function(keyEvent) {
        console.log('go en comuna');
        console.log(keyEvent);
        $scope.focusElementDireccion=true;
    };
    $scope.keyPressedDireccion = function(keyEvent) {
        if (keyEvent.keyCode == 13) {
            console.log('go en direccion');
            $scope.focusElementObservacion=true;
        }
    }; */
    /* $scope.keyPressedObservacion = function(keyEvent) {
        if (keyEvent.keyCode == 13) {
            console.log('go en observacion');
            $scope.focusElement="observacion";
        }
    }; */

      
}); // End of menu toggle controller.