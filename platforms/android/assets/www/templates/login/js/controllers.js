// Controller of menu toggle.
// Learn more about Sidenav directive of angular material
// https://material.angularjs.org/latest/#/demo/material.components.sidenav
appControllers.controller('LoginCtrl', function ($scope, $timeout, $mdUtil,$http, $ionicPopup, $ionicModal, $mdSidenav, $log, $ionicHistory, $state, $ionicPlatform, $mdDialog, $mdBottomSheet, $mdMenu, $mdSelect, $ionicLoading, UserDat,$cordovaOauth,HostService,$q) {
    
    $scope.toggleLeft = buildToggler('left');
    console.log("fasdfasfd");
    // buildToggler is for create menu toggle.
    // Parameter :  
    // navID = id of navigation bar.
    function buildToggler(navID) {
        var debounceFn = $mdUtil.debounce(function () {
            $mdSidenav(navID).toggle();
        }, 0);
        return debounceFn;
    };// End buildToggler.

    // navigateTo is for navigate to other page 
    // by using targetPage to be the destination state. 
    // Parameter :  
    // stateNames = target state to go
    $scope.navigateTo = function (stateName) {
        $timeout(function () {
            $mdSidenav('left').close();
            if ($ionicHistory.currentStateName() != stateName) {
                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                });
                $state.go(stateName);
            }
        }, ($scope.isAndroid == false ? 300 : 0));
    };// End navigateTo.

    //closeSideNav is for close side navigation
    //It will use with event on-swipe-left="closeSideNav()" on-drag-left="closeSideNav()"
    //When user swipe or drag md-sidenav to left side
    $scope.closeSideNav = function(){
        $mdSidenav('left').close();
    };
    //End closeSideNav

    //  $ionicPlatform.registerBackButtonAction(callback, priority, [actionId])
    //
    //     Register a hardware back button action. Only one action will execute
    //  when the back button is clicked, so this method decides which of
    //  the registered back button actions has the highest priority.
    //
    //     For example, if an actionsheet is showing, the back button should
    //  close the actionsheet, but it should not also go back a page view
    //  or close a modal which may be open.
    //
    //  The priorities for the existing back button hooks are as follows:
    //  Return to previous view = 100
    //  Close side menu         = 150
    //  Dismiss modal           = 200
    //  Close action sheet      = 300
    //  Dismiss popup           = 400
    //  Dismiss loading overlay = 500
    //
    //  Your back button action will override each of the above actions
    //  whose priority is less than the priority you provide. For example,
    //  an action assigned a priority of 101 will override the ‘return to
    //  previous view’ action, but not any of the other actions.
    //
    //  Learn more at : http://ionicframework.com/docs/api/service/$ionicPlatform/#registerBackButtonAction


    $ionicPlatform.registerBackButtonAction(function(){

        if($mdSidenav("left").isOpen()){
            //If side navigation is open it will close and then return
            $mdSidenav('left').close();
        }
        else if(jQuery('md-bottom-sheet').length > 0 ) {
            //If bottom sheet is open it will close and then return
            $mdBottomSheet.cancel();
        }
        else if(jQuery('[id^=dialog]').length > 0 ){
            //If popup dialog is open it will close and then return
            $mdDialog.cancel();
        }
        else if(jQuery('md-menu-content').length > 0 ){
            //If md-menu is open it will close and then return
            $mdMenu.hide();
        }
        else if(jQuery('md-select-menu').length > 0 ){
            //If md-select is open it will close and then return
            $mdSelect.hide();
        }

        else{

            // If control :
            // side navigation,
            // bottom sheet,
            // popup dialog,
            // md-menu,
            // md-select
            // is not opening, It will show $mdDialog to ask for
            // Confirmation to close the application or go to the view of lasted state.

            // Check for the current state that not have previous state.
            // It will show $mdDialog to ask for Confirmation to close the application.

            if($ionicHistory.backView() == null){

                //Check is popup dialog is not open.
                if(jQuery('[id^=dialog]').length == 0 ) {

                    // mdDialog for show $mdDialog to ask for
                    // Confirmation to close the application.

                    $mdDialog.show({
                        controller: 'DialogController',
                        templateUrl: 'confirm-dialog.html',
                        targetEvent: null,
                        locals: {
                            displayOption: {
                                title: "Confirmación",
                                content: "¿Quiere salir de la aplicación?",
                                ok: "CONFIRMAR",
                                cancel: "CANCELAR"
                            }
                        }
                    }).then(function () {
                        //If user tap Confirm at the popup dialog.
                        //Application will close.
                        ionic.Platform.exitApp();
                    }, function () {
                        // For cancel button actions.
                    }); //End mdDialog
                }
            }
            else{
                //Go to the view of lasted state.
                $ionicHistory.goBack();
            }
        }

    },100);
    //End of $ionicPlatform.registerBackButtonAction

    
    $scope.data = {};
    $scope.typelogin = "lg";
    console.log($scope.textdata,"$scope.textdata");
    console.log("app intro");


    //Entorno de QA
    // var basedrupal = "http://qa.consumovehicular.cl/";

    //Entorno de Produccion
    var basedrupal = "http://www.consumovehicular.cl/";
    
    /* if (window.localStorage.getItem("token")!="" && window.localStorage.getItem("token")!=undefined) {
            if(window.localStorage.getItem("tutorial")!=undefined || window.localStorage.getItem("tutorial"))
                $scope.navigateTo("app.etiquetas");
            else
                $scope.navigateTo('app.home');
            //navigateTo(DatApp.screen_main.action);
        }  */
    

    $scope.loginUser = function() {
         $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
        });
        
        var message = "Los siguientes campos son Obligatorios:";
                    var validate= false;
                    if($scope.data.username=="")
                    {
                        message+=" Nombre de Usuario ";
                        validate = true;
                    }
                    if($scope.data.userpass=="")
                    {
                        message+=" Contraseña ";
                        validate=true
                    }
                    if(validate){
                        message+="!";
                        $ionicLoading.hide();
                        $scope.showPopup('Inicio de Sesión',message);
                    }
                    else
                    {
                        //$scope.showPopup("se ejecuta loginUser y estan llenos los campos");
                        $scope.typelogin="lg";
                        $scope.getToken($scope.data.username,$scope.data.userpass,"lg");
                        //loginUserDrupal($scope.data.username,$scope.data.userpass,"lg");
                    }
                
    }

    function loginUserQuery(user,pass,type,token){
        token = token.substring(1,token.length - 1);
        var settings = {
            "url": basedrupal + "usrapi/user/login",
            "method": "POST",
            "headers": {
                "content-type":"application/json",
                "cache-control":"no-cache",
                "x-csrf-token": token
            },
            "data": JSON.stringify ({
                "name": user,
                "pass": pass
            }),
            success:handleSuccessLogin,
            error:handleErrorLogin
        }
        $.ajax(settings);
    }
        
//"content-type": "application/x-www-form-urlencoded; charset=UTF-8",
    function loginUserDrupal(user,pass,type){

            //Ejecuto la busqueda de token
            $scope.getToken();
            var crfToken = $scope.tokenLogin;
            //$scope.showPopup("token:",crfToken.substring(3,crfToken.length - 1));
            crfToken = crfToken.substring(3,crfToken.length - 1);
            //Hago login con token obtenido
            $scope.showPopup("login:"+user+" "+pass+" "+type," crfToken: "+ "\"" + crfToken.toString() + "\"");
            var settings = {
                "url": basedrupal + "usrapi/user/login",
                "method": "POST",
                "headers": {
                    "content-type":"application/json",
                    "cache-control":"no-cache",
                    "x-csrf-token": crfToken
                },
                "data": JSON.stringify ({
                    "name": user,
                    "pass": pass
                }),
                success:handleSuccessLogin,
                error:handleErrorLogin
            }
            $.ajax(settings);
            //$.ajax(settings).done(handleSuccessLogin).error(handleErrorLogin);
    }
    

    function handleErrorLogin( response ) {
        //$scope.showPopup("error login drupal",response.data);
        $ionicLoading.hide();

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": basedrupal+"usrapi/user/logout",
            "method": "POST",
            "headers": {
              "x-csrf-token": $scope.tokenLogin,
              "content-type": "application/json",
              "cache-control": "no-cache"
              }
          }
            $.ajax(settings).done(function (response) {
            console.log(response,"logout");
            });
          
                
        if(response.status==401 && $scope.typelogin=="lg"){
                $scope.showPopup('','Usuario o Contraseña incorrecto.');
        }
        if(response.status==401 && $scope.typelogin!="lg"){
            console.log(UserDat,"antes del registro");
            var settings = {
              "url": basedrupal + "usrapi/user/register",
              "method": "POST",
              "headers": {
                "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                "cache-control": "no-cache",
              },
              "data": {
                "name": UserDat.user,
                "mail": UserDat.mail,
                "pass": UserDat.id,
                "field_nombre_completo[und][0][value]": UserDat.fullname
              }
            }
            $.ajax(settings).done(function (response) {
                loginUser(UserDat.user,UserDat.id,$scope.typelogin);
              
              
            }).error(function (response) {
                console.log(response,"error login");
                if(response.status==406 ){
                    $scope.showPopup('Registro','Su cuenta de correo ya se encuentra registrada.');
                }
                else{
                    $scope.showPopup('','Fallo del registro, por favor intente de nuevo.');
                }
            });
                
        }
    }

    function handleSuccessLogin( response ) {
        //$scope.showPopup("handleSuccessLogin login drupal",response.status + " " + response.message);
        $ionicLoading.hide();

        console.log(response, "success");
            window.localStorage.setItem("sessid", response.sessid);
            window.localStorage.setItem("session_name", response.session_name);
            window.localStorage.setItem("token", response.token);
            //window.localStorage.setItem("tutorial",false);
            UserDat.user=response.user.name;     
           UserDat.name=response.user.name;
           UserDat.fullname=response.user.field_nombre_completo.und[0].value;
           UserDat.mail=response.user.mail;
           UserDat.id=response.user.uid;
           UserDat.typelogin="lg";
           window.localStorage.setItem("UserDat", JSON.stringify(UserDat));
           window.localStorage.setItem("UserD", UserDat.user);
           window.localStorage.setItem("typelogin", "lg");
           console.log(UserDat,"user ddaadad");
          UserDat.idToken = response.token;
          console.log("login");
          //$scope.showPopup("token de handlesuccesslogin: ",response.idToken);
           /* if(window.localStorage.getItem("tutorial")!=undefined || window.localStorage.getItem("tutorial"))
                $scope.navigateTo("app.etiquetas");
            else
                $scope.navigateTo('app.home'); */

            //$scope.navigateTo(DatApp.screen_main.action);
            $scope.navigateTo('app.home');
    }

    $scope.getToken = function(user,pass,type){
        var settings = {
            "url": basedrupal + "services/session/token",
            "method": "GET",
            "headers": {
                "postman-token": "54733c33-4918-811b-3987-69d6edeaa3a0"
              }
          }
          $.ajax(settings).done(function (response) {
            console.log('escribe el success');
            $scope.tokenLogin = response.toString();
            loginUserQuery(user,pass,type,response.toString());
        }
        ).fail(console.log('escribe el error'));
    }


    $scope.loginGP = function () {
        console.log("googlep1");
        // $cordovaOauth.google("1026488141013-5e6priqolj1ee765lrr18tf02vf8kjst.apps.googleusercontent.com", ["email"]).then(function(result) {
        $cordovaOauth.google("963682753463-o2nvc37on866i0plejul13c8tm104o3b.apps.googleusercontent.com", ["email"]).then(function(result) {
            console.log("googlep2");
                UserDat.idToken = result.access_token;
                console.log(result,"google");
                if(result.access_token !== "") {
                    var term=null;
                    $.ajax({
                           url:'https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token='+UserDat.idToken,
                           type:'GET',
                           data:term,
                           dataType:'json',
                           error:function(jqXHR,text_status,strError){
                           },
                           success:function(data)
                           {
                            console.log(data,"googledata");
                            UserDat.user="oauthconnector_google__"+data.id;
                            UserDat.typelogin="gp";
                            UserDat.name=data.given_name;
                            if(UserDat.name==""){UserDat.name=data.email};
                            UserDat.fullname=data.name;
                            if(UserDat.fullname==""){UserDat.fullname=data.email}
                            UserDat.mail=data.email;
                            UserDat.id=data.id;
                            UserDat.typelogin="gp";
                            console.log(UserDat,"datos usuario");
                            window.localStorage.setItem("UserDat", JSON.stringify(UserDat));
                            window.localStorage.setItem("token", UserDat.idToken);
                            window.localStorage.setItem("UserD", UserDat.user);
                            window.localStorage.setItem("typelogin", "gp");
                            //window.localStorage.setItem("tutorial",true);
                            $scope.typelogin="gp";
                            $ionicLoading.show({
                                template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
                            });
                            //loginUser(UserDat.user,UserDat.id,$scope.typelogin);
                            //$scope.navigateTo("app.denuncia");
                            $ionicLoading.hide();
                            if(window.localStorage.getItem("tutorial")!=undefined || window.localStorage.getItem("tutorial"))
                                $scope.navigateTo("app.etiquetas");
                            else
                                $scope.navigateTo('app.home');

                           }
                        })
                      
                    } else {
                            $ionicLoading.hide();
                            alert("No se Completo el registro intente de nuevo");
                            $state.go('login', {}, {reload: true});
                        }
            }, function(error) {
              //  alert("Auth Failed..!!"+error);
            });
      };

      $scope.Loginfb = function(){
        console.log("clicked");
        $cordovaOauth.facebook("2010397785905675",["email", "read_stream", "user_website", "user_location", "user_relationships"]).then(function(result) {
        //$cordovaOauth.facebook("121907685214028",["email", "user_website"]).then(function(result) {   
            console.log(result,"facebook");
            UserDat.idToken = result.access_token;
            //if($localStorage.hasOwnProperty("accessToken") === true) {
               if(result.access_token !== "") { 
                $http.get("https://graph.facebook.com/v2.10/me", { params: { access_token: UserDat.idToken, fields: "email,name", format: "json" }}).then(function(result) {
                    UserDat.name = result.data.name;
                    if(UserDat.name==""){UserDat.name=result.data.email};
                    UserDat.user = "oauthconnector_facebook__"+result.data.id;
                    UserDat.fullname=result.data.name;
                    if(UserDat.fullname==""){UserDat.fullname=result.data.email};
                    UserDat.mail=result.data.email;
                    UserDat.id=result.data.id;
                    UserDat.typelogin="fb";
                    $scope.typelogin="fb";
                    window.localStorage.setItem("UserDat", JSON.stringify(UserDat));
                    window.localStorage.setItem("token", UserDat.idToken);
                    window.localStorage.setItem("UserD", result.data.name);
                    window.localStorage.setItem("tutorial",true);
                    $ionicLoading.show({
                        template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
                    });
                    //$scope.navigateTo("app.denuncia");
                    $ionicLoading.hide();
                    if(window.localStorage.getItem("tutorial")!=undefined  || window.localStorage.getItem("tutorial"))
                        $scope.navigateTo("app.etiquetas");
                    else
                        $scope.navigateTo('app.home');

                    //loginUser(UserDat.user,UserDat.id,$scope.typelogin);
                }, function(error) {
                    $ionicLoading.hide();
                    alert("No se Completo el registro intente de nuevo");
                    console.log(error);
                });
            } else {
                $ionicLoading.hide();
                alert("No se Completo el registro intente de nuevo");
                $location.path("/login");
            }
            
         }, function(error) {
            alert("Auth Failed..!!"+error);
         });
     };



   $ionicModal.fromTemplateUrl('templates/login/html/registro.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalregistro = modal;
    });

     $scope.registrar = function(car) {
        $scope.modalregistro.show();
    };

    $scope.closeModalregsitro = function() {
      $scope.modalregistro.hide();
    };

  


     $ionicModal.fromTemplateUrl('templates/login/html/changepass.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalpass = modal;
    });

     $scope.changePassword = function(car) {
        $scope.modalpass.show();
    };

    $scope.closeModalpass = function() {
      $scope.modalpass.hide();
    };

    


    $scope.registro = {
        name : "",
        pass :  "",
        passc: "",
        mail : "" ,
        fullname: ""

    }

    $scope.changepass = {
        name:"",
        mail:""
    }
    $scope.recuperarpass = function(){
        var message = "Debe indicar el email.";
        var validate= false;
        // if($scope.changepass.name=="" && $scope.changepass.mail=="")
        if($scope.changepass.mail=="")
        {
            validate = true;
        }
        if(validate){
                        message+="!";
                        $scope.showPopup('Recuperar',message);
                    }
                    else
                    {
                        var settings = {
                                  "async": true,
                                  "crossDomain": true,
                                  "url": basedrupal + "usrapi/user/request_new_password",
                                  "method": "POST",
                                  "headers": {
                                    "x-csrf-token": window.localStorage.getItem("token"),
                                    "content-type": "application/x-www-form-urlencoded",
                                    "cache-control": "no-cache",
                                    "postman-token": "6cda7c1b-0a15-c2b8-a2f1-cd78a25b0b1f"
                                  },
                                  "data": {
                                    "name": $scope.changepass.mail,
                                    "mail": $scope.changepass.mail
                                  }
                                }

                                $.ajax(settings).done(function (response) {
                                  //  console.log(response,"recueprar");
                                  $scope.showPopup('Restablecer','Se enviara un correo parar restablecer su contraseña.');
                                  $scope.closeModalpass();
                                }).error(function (response) {
                                  //  console.log(response,"recueprar");
                                  $scope.showPopup('Recuperar Contraseña','Usuario o email incorrecto.');
                                  //$scope.closeModalpass();
                                });
                    }
    }


    $scope.RegisterUser = function(form) {

                if($scope.registro.pass != $scope.registro.passc){
                    $scope.showPopup('Registro',"No coinciden las contraseñas.");
                }
                else
                {
                    var message = "Los siguientes campos son Obligatorios:";
                    var validate= false;
                    if($scope.registro.fullname=="")
                    {
                        message+=" Nombre Completo ";
                        validate = true;
                    }
                    if($scope.registro.pass=="")
                    {
                        message+=" Contraseña ";
                        validate=true
                    }
                    if($scope.registro.mail==undefined)
                    {
                        message+=" Email Incorrecto ";
                        validate=true
                    }
                    if(validate){
                        message+="!";
                        $scope.showPopup('Registro',message);
                    }
                    else
                    {
                        //console.log($scope.registro,"anes");
                        var settings = {
                          "url": basedrupal + "usrapi/user/register",
                          "method": "POST",
                          "headers": {
                            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
                            "cache-control": "no-cache",
                          },
                          "data": {
                            "name": $scope.registro.mail,
                            "mail": $scope.registro.mail,
                            "pass": $scope.registro.pass,
                            "field_nombre_completo[und][0][value]": $scope.registro.fullname
                          }
                        }
                        console.log(settings,"antes de registrar");
                        $.ajax(settings).done(function (response) {
                            console.log(response,"registro success");
                            $scope.showPopup('Registro','Su registro fue exitoso.');
                            $scope.closeModalregsitro();
                        }).error(function (response) {
                            console.log(response,"error login");
                            if(response.status==406 ){
                                $scope.showPopup('Registro','Su cuenta de correo ya se encuentra registrada.');
                            }
                            else{
                                $scope.showPopup('Login','Fallo del registro, por favor intente de nuevo.');
                            }
                        });
                    }
                }
      };

     $scope.showPopup = function(title,template) {
        var alertPopup = $ionicPopup.alert({
            title: title,
            template: template,
            cancelText:"Cancelar",
            okText: "Aceptar"
        });
    }

 $scope.loginpggpp = function () {
     $scope.isLoading=false;
        if ($scope.isLoading == false) {
            $scope.isLoading = true;
            console.log("adfafasdfafasdfasdfasdfaf afasfasdfasf afasd fasdf");
            // Calling $cordovaOauth.google for login google.
            // Format:
            // $cordovaOauth.google(CLIENT_ID,[GOOGLE_PERMISION]) 
            // For CLIENT_ID is window.globalVariable.oAuth.googlePlus from www/js/app.js at globalVariable session.
            $cordovaOauth.google(window.globalVariable.oAuth.googlePlus
                , ["https://www.googleapis.com/auth/urlshortener",
                    "https://www.googleapis.com/auth/userinfo.email"]).then(function (result) {
                    //After call cordovaOauth.google it will return access_token for you to calling google API.

                    $scope.accessToken = result.access_token;
                    // Calling http service for getting user profile from google.
                    // By send parameter access_token, format.
                    $http.get("https://www.googleapis.com/oauth2/v1/userinfo", {
                        params: {
                            access_token: result.access_token,
                            format: "json"
                        }
                    }).then(function (result) {
                        // Success retrieve data by calling http service.
                        // Store user profile information from google API to userInfo variable.
                        $scope.userInfo = {
                            name: result.data.name,
                            email: result.data.email,
                            link: result.data.link,
                            pictureProfileUrl: result.data.picture,
                            gender: result.data.gender,
                            id: result.data.id,
                            access_token: $scope.accessToken
                        };
                        // Store user profile information to localStorage service.
                        localStorage.set("GooglePlus", $scope.userInfo);
                        // Navigate to google plus profile page.
                        $scope.navigateTo("app.googlePlusProfile");
                    });

                }
                , function (error) {
                    // Error retrieve data.
                    console.log(error);
                });
            $scope.isLoading = false;
        }
    };// End login.


    //Se ingresa el login nativo de facebook
    var fbLoginSuccess = function(response) {
        if (!response.authResponse){
          fbLoginError("Cannot find the authResponse");
          return;
        }
    
        var authResponse = response.authResponse;
    
        if(response.authResponse.accessToken !== "") { 
            $http.get("https://graph.facebook.com/v2.10/me", { params: { access_token: response.authResponse.accessToken, fields: "email,name", format: "json" }}).then(function(result) {
                UserDat.name = result.data.name;
                if(UserDat.name==""){UserDat.name=result.data.email};
                UserDat.user = "oauthconnector_facebook__"+result.data.id;
                UserDat.fullname=result.data.name;
                if(UserDat.fullname==""){UserDat.fullname=result.data.email};
                UserDat.mail=result.data.email;
                UserDat.id=result.data.id;
                UserDat.typelogin="fb";
                $scope.typelogin="fb";
                window.localStorage.setItem("UserDat", JSON.stringify(UserDat));
                window.localStorage.setItem("token", response.authResponse.accessToken);
                window.localStorage.setItem("UserD", result.data.name);
                window.localStorage.setItem("tutorial",true);
                window.localStorage.setItem("typelogin", "fb");
                $ionicLoading.show({
                    template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
                });
                //$scope.navigateTo("app.denuncia");
                $ionicLoading.hide();
                /*if(window.localStorage.getItem("tutorial")!=undefined  || window.localStorage.getItem("tutorial"))
                    $scope.navigateTo("app.etiquetas");
                else*/
                    $scope.navigateTo('app.home');

                //loginUser(UserDat.user,UserDat.id,$scope.typelogin);
            }, function(error) {
                $ionicLoading.hide();
                alert("No se Completo el registro intente de nuevo");
                console.log(error);
            });
        }
      };
    
      // This is the fail callback from the login method
      var fbLoginError = function(error){
        console.log('fbLoginError', error);
        $ionicLoading.hide();
      };
    
      // This method is to get the user profile info from the facebook api
      var getFacebookProfileInfo = function (authResponse) {
        var info = $q.defer();
    
        facebookConnectPlugin.api('/me?fields=email,name&access_token=' + authResponse.accessToken, null,
          function (response) {
                    console.log(response);
            info.resolve(response);
          },
          function (response) {
                    console.log(response);
            info.reject(response);
          }
        );
        return info.promise;
      };
    
      //This method is executed when the user press the "Login with facebook" button
      $scope.facebookSignIn = function() {
        facebookConnectPlugin.getLoginStatus(function(success){
          if(success.status === 'connected'){
            if(success.authResponse.accessToken !== "") { 
                $http.get("https://graph.facebook.com/v2.10/me", { params: { access_token: success.authResponse.accessToken, fields: "email,name", format: "json" }}).then(function(result) {
                    UserDat.name = result.data.name;
                    if(UserDat.name==""){UserDat.name=result.data.email};
                    UserDat.user = "oauthconnector_facebook__"+result.data.id;
                    UserDat.fullname=result.data.name;
                    if(UserDat.fullname==""){UserDat.fullname=result.data.email};
                    UserDat.mail=result.data.email;
                    UserDat.id=result.data.id;
                    UserDat.typelogin="fb";
                    $scope.typelogin="fb";
                    window.localStorage.setItem("UserDat", JSON.stringify(UserDat));
                    window.localStorage.setItem("token", success.authResponse.accessToken);
                    window.localStorage.setItem("UserD", result.data.name);
                    window.localStorage.setItem("tutorial",true);
                    $ionicLoading.show({
                        template: '<div class="loader"><svg class="circular"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
                    });
                    //$scope.navigateTo("app.denuncia");
                    $ionicLoading.hide();
                    if(window.localStorage.getItem("tutorial")!=undefined  || window.localStorage.getItem("tutorial"))
                        $scope.navigateTo("app.etiquetas");
                    else
                        $scope.navigateTo('app.home');

                    //loginUser(UserDat.user,UserDat.id,$scope.typelogin);
                }, function(error) {
                    $ionicLoading.hide();
                    alert("No se Completo el registro intente de nuevo");
                    console.log(error);
                });
            } else {
                $ionicLoading.hide();
                alert("No se Completo el registro intente de nuevo");
                $location.path("/login");
            }
          } else {
            // If (success.status === 'not_authorized') the user is logged in to Facebook,
                    // but has not authenticated your app
            // Else the person is not logged into Facebook,
                    // so we're not sure if they are logged into this app or not.
    
                    //console.log('getLoginStatus', success.status);
    
                    /*$ionicLoading.show({
                        template: 'Logging in...'
                    });*/
    
                    // Ask the permissions you need. You can learn more about
                    // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
            facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
            //$scope.navigateTo('app.home');
          }
        });
      };
    //fin de login nativo de facebook
}); // End of menu toggle controller.