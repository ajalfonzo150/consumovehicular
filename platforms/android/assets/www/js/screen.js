angular.module('AppStands.screen', [])

.provider('screen', function () {
  var screen = {
  "id":"app.profile",
  "name": "Profile",
  "template":"profile",
  "controller":"ProfileCtrl",
  "menufloat": {
    "active": false,
    "options": {
      "1": {
        "name": "Login",
        "action": "app.login"
      },
      "2": {
        "name": "ProFile",
        "action": "app.profile"
      },
      "3": {
        "name": "Cars",
        "action": "app.cars"
      }
    }
  },
  "tipo": "social",
  "textbtn": "",
  "action": "app.profile",
  "profile": {
        "imgbanner": "",
        "imgprof": "",
        "title": "",
        "subtitle": "",
        "imgopt1": "",
        "option1": "",
        "actionopt1": "",
        "imgopt2": "",
        "option2": "",
        "actionopt2": "",
        "imgopt3": "",
        "option3": "",
        "actionopt3": "",
        "namelist": "",
        "titlelist": "",
        "deslist": "",
        "imgleftlist": "",
        "imgrightlist": "",
        "actionlist": "",
        "btnaction": "",
        "imgbtn": "",
        "activebtn": true
      },
  "tab": [
    {
      "nametab": "",
      "icontab": "",
      "action": ""
    }
  ],
  "list": {
    "namelst": "demo",
    "titlelst": "",
    "desclst": "",
    "imgleftlst": "",
    "imgrightlst": "",
    "clicklst": "",
    "modal": false
  },
  "form": {
    "": {
      "textff": "",
      "typeff": [
        "check",
        "text",
        "num",
        "ddl"
      ],
      "modelff": "",
      "order": ""
    }
  }
};
  return {
    setScreen: function (value) {
      screen = value;
    },
    getScreen: function () {
      return {
          config_screen  : screen
      }
    },
    $get: function () {
      return {
          config_screen  : screen
      }
    }
  }
});

