
appServices.service('HostService', function( $http, $q, $ionicLoading ) {
    

    function loading(){
        $ionicLoading.show({
            template: '<div class="loader"><svg class="circular"><circle class="path" cx="30" cy="30" r="10" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg>Cargando...</div>'
        });

        // For example's sake, hide the sheet after two seconds
    
    };

    function loadingHide(){
        $ionicLoading.hide();
    
    };

    //Entorno de QA
    // var basedrupal = "http://qa.consumovehicular.cl/";
    // var baseurl = "http://qa.consumovehicular.cl:8080/scv/"; //Entorno de produccion middleware java

    //Entorno de Produccion
    var basedrupal = "http://www.consumovehicular.cl/";
    var baseurl = "http://www.consumovehicular.cl/backend/scv/"; //Entorno de produccion middleware java
    

     function saveDenuncia(obj) {
        loading();
         var request = $http({
             method: "post",
             url: baseurl + "denuncia",
             data:JSON.stringify(obj)
             
         });
         return( request.then( handleSuccess, handleError ) );
     }


    function getMarcas() {
        loading();
        var request = $http({
            method: "get",
            url: baseurl + "vehiculo/marcas"
        });
        return( request.then( handleSuccess, handleError ) );
    }


     function getMarcasMedia() {
        loading();
        var request = $http({
            method: "get",
            url: baseurl + "vehiculoprivado/marcas?consumo=true&sort=nombreMarca"
        });
        return( request.then( handleSuccess, handleError ) );
    }


    function getModelo(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl + "vehiculo/modelos",
            params: {
                idMarca : id
            },
            data :{
                idMarca : id
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function getModeloMedia(id) {
        loading();
        console.log(baseurl + "vehiculoprivado/modelos?idMarca="+id,"moelos");
        var request = $http({
            method: "get",
            url: baseurl + "vehiculoprivado/modelos?idMarca="+id,
            params: {
                idMarca : id,
                consumo: true
            },
            data :{
                idMarca : id,
                consumo : true
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function getAllCars2(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl + "vehiculo",
            params: {
                idModelo : id
            },
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

     function getAllCars(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl + "vehiculo?idModelo="+id,
            params: {
                idModelo : id
            },
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

     function getAllCarsTT(id) {
            loading();
            var settings = {
              "async": true,
              "crossDomain": true,
              "url": "http://serverdesa.emergya.cl:65080/scv/vehiculo?idModelo="+id,
              "method": "GET",
              "headers": {
                "cache-control": "no-cache",
                "postman-token": "c6920abf-6d7c-8306-4b89-30ebc0b6e9ce"
              }
            }           

            return ($.ajax(settings).done(handleSuccess).error(handleError));
        }


    function getAllCarsM(id,year) {
            loading();
            var request = $http({
                method: "get",
                url: baseurl + "vehiculoprivado?yearVehiculo="+year,
               headers: {
                    "cache-control": "no-cache"
              },
              params: {
                    idModelo : id,
                    consumo : true
                }
            });
        return( request.then( handleSuccess, handleError ) );
    }

    function getCarrocerias(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"vehiculo/carrocerias",
            params: {
                idModelo : id
            },
            data:{
                idMarca : id
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function getCarroceriasMedia(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"vehiculoprivado/carrocerias",
            params: {
                idModelo : id,
                consumo : true
            },
            data:{
                idMarca : id,
                consumo : true
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function getCombustible(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"vehiculo/etiquetas?idModelo="+id,
            params:{
                IdModelo : id
            },
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function getCombustibleM(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"vehiculoprivado/etiquetas?consumo=true&idModelo="+id,
            params:{
                IdModelo : id,
                consumo : true
            },
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }


    function getYearM(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"consumo/yearvehiculos?idModelo="+id,
            params:{
                idModelo:id,
                consumo : true
            },
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function getCarByUser(id) {
        console.log(id);
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"consumo/vehiculo/",
            params:{
                idUsuario:id
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function DeleteCar(obj) {
        loading();
        var request = $http({
            method: "DELETE",
            url: baseurl +"consumo/vehiculo/"+obj.idVehiculoUsuario,
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }

    function DeleteConsumo(obj) {
        loading();
        var request = $http({
            method: "DELETE",
            url: baseurl +"consumo/"+obj.idConsumo,
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }


    function getConsumo(id) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"consumo/"+id
        });
        return( request.then( handleSuccess, handleError ) );
    }


    function getMiMedia(user,idVehiculo,yearV) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"consumo/media/"+user+"?idVehiculoUsuario="+idVehiculo,
            params:{
                idVehiculo:idVehiculo,
                year:yearV
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }


    function getMediabyCar(idCar,year) {
        loading();
        var request = $http({
            method: "get",
            url: baseurl +"consumo/media?idVehiculo="+idCar+"&year=" + year,
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
        });
        return( request.then( handleSuccess, handleError ) );
    }


    function getParamsCompare() {
        console.log('adfadfsf');
        loading();
        var settings = {
              "async": true,
              "crossDomain": true,
              "url": basedrupal +"parametros/listado",
              "method": "GET",
              "headers": {
                "cache-control": "no-cache",
                "postman-token": "c6920abf-6d7c-8306-4b89-30ebc0b6e9ce"
              }
            }           

            return ($.ajax(settings).done(handleSuccess).error(handleError));
    }



    function saveCar(obj) {
        loading();
         var request = $http({
             method: "post",
             url: baseurl + "consumo/vehiculo",
             data:  JSON.stringify(obj)
         });
         return( request.then( handleSuccess, handleError ) );
     }

     function saveCar(obj) {
        loading();
         var request = $http({
             method: "post",
             url: baseurl + "consumo/vehiculo",
             data:  JSON.stringify(obj)
         });
         return( request.then( handleSuccess, handleError ) );
     }



    function saveConsumo(obj) {
        loading();
        console.log(JSON.stringify(obj),"obj");
         var request = $http({
             method: "post",
             url: baseurl + "consumo",
             data:  JSON.stringify(obj),
            headers: {
            "Content-Type": "application/json;charset=utf-8"
            }
         });
         return( request.then( handleSuccess, handleError ) );
     }


     
    

    function handleError( response ) {
        loadingHide();
        console.log(response, "Error"); //status =0 cuando devuelve error
        if (
            ! angular.isObject( response.data ) ||
            ! response.data.message
            ) {
            return( $q.reject( "An unknown error occurred." ) );
        }
        return( $q.reject( response.data.message ) );
    }

    function handleSuccess( response ) {
        loadingHide();
        console.log(response, "success");
        return( response.data );
    }

 

    

    //metodos
     return({
                    getMarcas: getMarcas,
                    getModelo: getModelo,
                    getCarrocerias: getCarrocerias,
                    getCombustible: getCombustible,
                    saveDenuncia : saveDenuncia,
                    getAllCars : getAllCars,
                    saveCar:saveCar,
                    getCarByUser:getCarByUser,
                    getConsumo : getConsumo,
                    DeleteCar:DeleteCar,
                    saveConsumo:saveConsumo,
                    getMiMedia:getMiMedia,
                    getMediabyCar:getMediabyCar,
                    DeleteConsumo:DeleteConsumo,
                    getParamsCompare:getParamsCompare,
                    getMarcasMedia:getMarcasMedia,
                    getModeloMedia:getModeloMedia,
                    getCombustibleM:getCombustibleM,
                    getCarroceriasMedia:getCarroceriasMedia,
                    getYearM:getYearM,
                    getAllCarsM:getAllCarsM
                    
                });

});


