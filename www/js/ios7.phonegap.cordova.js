// Pre-requisites:
// 1. Device core plugin
// 2. Splashscreen core plugin (3.1.0)
// 3. config.xml:  <preference name="AutoHideSplashScreen" value="false" />
// 4. config.xml:  <preference name="DisallowOverscroll" value="true" />

function onDeviceReady() {
    if (parseFloat(window.device.version) >= 7.0) {
          document.body.style.marginTop = "20px";
          // OR do whatever layout you need here, to expand a navigation bar etc
    }
    navigator.splashscreen.hide();
}


document.addEventListener('deviceready', onDeviceReady, false);
